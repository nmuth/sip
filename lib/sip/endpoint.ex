defmodule Sip.Endpoint do
  defmacro __using__(opts) do
    router_mod = Keyword.get(opts, :router)
    port = Keyword.get(opts, :port, 4200)

    quote location: :keep do
      use Plug.Router

      plug(Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Jason)

      plug(:match)
      plug(:dispatch)

      forward "/", to: unquote(router_mod)

      def child_spec opts do
        %{
          id: __MODULE__,
          start: {__MODULE__, :start_link, [opts]}
        }
      end

      def start_link _opts do
        IO.puts "starting server at http://localhost:#{unquote(port)}/"

        Plug.Adapters.Cowboy.http(__MODULE__, [], port: unquote(port))
      end
    end
  end
end
