defmodule Sip.Client do
  def get!(path, opts \\ []) do
    HTTPoison.get!(get_url(path))
    |> process_resp(opts)
  end

  def post!(path, data, opts \\ []) do
    HTTPoison.post!(get_url(path), Jason.encode!(data), [{"Content-Type", "application/json"}])
    |> process_resp(opts)
  end

  def put!(path, data, opts \\ []) do
    HTTPoison.put!(get_url(path), Jason.encode!(data), [{"Content-Type", "application/json"}])
    |> process_resp(opts)
  end

  def patch!(path, data, opts \\ []) do
    HTTPoison.patch!(get_url(path), Jason.encode!(data), [{"Content-Type", "application/json"}])
    |> process_resp(opts)
  end

  def delete!(path, opts \\ []) do
    HTTPoison.delete!(get_url(path), [{"Content-Type", "application/json"}])
    |> process_resp(opts)
  end

  def request!(method, path, body \\ "", opts \\ []) do
    HTTPoison.request!(method, get_url(path), body, [{"Content-Type", "application/json"}])
    |> process_resp(opts)
  end

  defp process_resp(%HTTPoison.Response{} = resp, opts) do
    content_type_json = Enum.find(
      resp.headers,
      fn {header, value} ->
	case String.downcase(header) do
	  "content-type" -> String.contains?(value, "application/json")
	  _ -> false
	end
      end
    )

    body = case content_type_json do
	     nil -> resp.body
	     _ -> Jason.decode!(resp.body)
	   end

    result = if resp.status_code < 400 do :ok else :error end

    if Keyword.get(opts, :full_response) do
      {result, resp.status_code, body, resp}
    else
      {result, resp.status_code, body}
    end
  end

  defp get_url path do
    "http://localhost:4200/#{path}"
  end
end
