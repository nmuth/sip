defmodule Sip.Route do
  require Plug.Router

  defmacro api(mod, fun, conn) do
    quote bind_quoted: [
      mod: mod,
      fun: fun,
      conn: conn,
    ] do
      conn = conn
      |> Plug.Conn.put_resp_content_type("application/json")

      case apply(mod, fun, [conn]) do
	{:ok, status_code, data} ->
	  conn
	  |> Plug.Conn.send_resp(status_code, Jason.encode!(data))

	other ->
	  conn
	  |> Plug.Conn.send_resp(500, Jason.encode!(%{ :internal_server_error => other }))
      end
    end
  end

  defmacro get(path, mod, fun, addl_args \\ []) do
    quote_match(:get, path, mod, fun, addl_args)
  end

  defmacro get(path, [do: yield]) do
    quote_match(:get, path, do: yield)
  end

  defmacro post(path, mod, fun, addl_args \\ []) do
    quote_match(:post, path, mod, fun, addl_args)
  end

  defmacro post(path, [do: yield]) do
    quote_match(:post, path, do: yield)
  end

  defmacro put(path, mod, fun, addl_args \\ []) do
    quote_match(:put, path, mod, fun, addl_args)
  end

  defmacro put(path, [do: yield]) do
    quote_match(:put, path, do: yield)
  end

  defmacro patch(path, mod, fun, addl_args \\ []) do
    quote_match(:patch, path, mod, fun, addl_args)
  end

  defmacro patch(path, [do: yield]) do
    quote_match(:patch, path, do: yield)
  end

  defmacro delete(path, mod, fun, addl_args \\ []) do
    quote_match(:delete, path, mod, fun, addl_args)
  end

  defmacro delete(path, [do: yield]) do
    quote_match(:delete, path, do: yield)
  end

  defmacro match(method, path, mod, fun, addl_args \\ []) do
    quote_match(method, path, mod, fun, addl_args)
  end

  defmacro match(method, path, [do: yield]) do
    quote_match(method, path, do: yield)
  end

  defp quote_match(method, path, mod, fun, addl_args \\ []) do
    quote do
      Plug.Router.match(unquote(path), via: unquote(method)) do
	#conn = var!(conn)
	#|> Plug.Conn.put_resp_content_type("application/json")

	unquote(quote_match_case(quote do: apply(unquote(mod), unquote(fun), unquote(addl_args))))
      end
    end
  end

  defp quote_match(method, path, do: yield) do
    quote do
      Plug.Router.match(unquote(path), via: unquote(method)) do
	#conn = var!(conn)
	#|> Plug.Conn.put_resp_content_type("application/json")

	unquote(quote_match_case(yield))
      end
    end
  end

  defp quote_match_case(expr) do
    quote do
      conn = var!(conn)

      case unquote(expr) do
	{:ok, data} ->
	  content_type = Sip.Route.get_content_type(data)
	  response_body = with "application/json" <- content_type,
			       json <- Jason.encode!(data) do
			    json
			  else
			    _ -> data
			  end

	  conn
	  |> Plug.Conn.put_resp_content_type(content_type)
	  |> Plug.Conn.send_resp(200, response_body)

	{:not_found} ->
	  conn
	  |> Plug.Conn.put_resp_content_type("text/plain")
	  |> Plug.Conn.send_resp(404, "")

	{_ok_or_error, status_code, data} ->
	  content_type = Sip.Route.get_content_type(data)
	  response_body = with "application/json" <- content_type,
			       json <- Jason.encode!(data) do
			    json
			  else
			    _ -> data
			  end

	  conn
	  |> Plug.Conn.put_resp_content_type(content_type)
	  |> Plug.Conn.send_resp(status_code, response_body)

	{:error, data} ->
	  IO.warn("internal server error: #{data}")
	  conn
	  |> Plug.Conn.put_resp_content_type("application/json")
	  |> Plug.Conn.send_resp(500, Jason.encode!(%{ error: :internal_server_error }))

	other ->
	  IO.warn "function returned unexpected result: #{inspect other}"
	  conn
	  |> Plug.Conn.put_resp_content_type("application/json")
	  |> Plug.Conn.send_resp(500, Jason.encode!(%{ error: :internal_server_error }))
      end
    end
  end

  defp quote_match_expr(mod, fun, addl_args) do
    quote do
      apply(unquote(mod), unquote(fun), [var!(conn), unquote_splicing(addl_args)])
    end
  end

  defp quote_match_expr(yield) do
    quote do
      unquote(yield)
    end
  end

  def get_content_type(result) when is_map(result) do
    "application/json"
  end
  def get_content_type(result) when is_list(result) do
    "application/json"
  end
  def get_content_type(_result) do
    "text/plain"
  end
end
