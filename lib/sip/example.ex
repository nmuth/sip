defmodule Sip.Web.Router do
  use Sip.Router

  Sip.Route.get("/", Sip.Web.Router, :example)

  Sip.Route.get "/asdf" do
    {:ok, "hello"}
  end

  def example do
    {:ok, "example"}
  end
end
