defmodule Sip.Router do
  defmacro __using__(_opts) do
    quote location: :keep do
      @before_compile Sip.Router
      use Plug.Router

      require Sip.Route

      plug(:match)
      plug(:dispatch)
    end
  end

  defmacro __before_compile__(_opts) do
    quote do
      match _ do
	send_resp(var!(conn), 404, "Resource not found")
      end
    end
  end
end
