defmodule Sip.MixProject do
  use Mix.Project

  def project do
    [
      app: :sip,
      version: "0.1.0",
      elixir: "~> 1.9-dev",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger, :httpoison]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:httpoison, "~> 1.5"},
      {:ex_doc, ">= 0.0.0", only: :dev},
    ]
  end

  defp description do
    """
    A simple framework for writing JSON APIs on top of Plug.
    """
  end

  defp package do
    [
      files: ~w(
	lib .formatter.exs mix.exs README* LICENSE* CHANGELOG*
      ),
      maintainers: ["Noah Muth"],
      licenses: ["Apache 2.0"],
      links: %{"GitLab" => "https://gitlab.com/nmuth/sip"}
    ]
  end
end
