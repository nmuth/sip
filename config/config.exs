use Mix.Config

config :sip, ecto_repos: [Sip.Repo]

config :sip, Sip.Repo,
  database: "sip_repo",
  username: "postgres",
  hostname: "127.0.0.1"
